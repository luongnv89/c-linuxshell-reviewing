#C-LinuxShell-Reviewing#
Some code for the exercises in my Computer Science course.

**C-Language**

Some code for the C-language excersices
- Erato 
- Fibonacci 
- Palidrome 
- Stack 
- Queue 
- ... 

**Linux-Shell**

Some code for the Linux-Shell excersices
- File Exist 
- File Type 
- Search Find 
- Show 
- Loop 
- ... 

**Lectures**

The lectures of course.
- C languages 
- Unix 
- Algorithms 

**Eric-Renault-TSP**

The solution of professor Eric Renault - Computer Science - Telecom Sudparis, France

-------------------------------
Nguyen Van Luong - luongnv89@gmail.com - [http://luongnv.info](http://luongnv.info)



